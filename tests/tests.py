import unittest
from . import ZinobeApp
import requests
import os

class TestZinobeAPP(unittest.TestCase):

    def test_validate_columns_from_datatable(self):
        z = ZinobeApp()
        table = z.create_table()
        columns =  table.columns.values.tolist()
        self.assertEqual(columns, ['Region', 'CityName', 'Languaje', 'Time'])

    def test_url_to_get_regions(self):
        response = requests.request("GET", ZinobeApp.url, headers=ZinobeApp.headers) 
        self.assertEqual(200, response.status_code)
        
    def test_url_to_get_countries(self):
        response = requests.request("GET", ZinobeApp.url, headers=ZinobeApp.headers)
        list_regions = list(map(lambda x: x['region'],response.json()))
        response = requests.request("GET", ZinobeApp.url_countries.format(list_regions[0]),
                                    headers=ZinobeApp.headers) 
        self.assertEqual(200, response.status_code)

    def test_validate_id_create_json(self):
        z = ZinobeApp()
        table = z.create_table()
        z.create_json(table)
        self.assertEqual(True, os.path.exists(r'../app/files/data.json'))

if __name__ == '__main__':
    unittest.main()