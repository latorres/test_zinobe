import requests
from iso639 import languages
import pandas as pd
from timeit import timeit
import sqlite3
import logging
import os
import hashlib

class ZinobeApp:
    url = "https://restcountries-v1.p.rapidapi.com/all"
    url_countries = "https://restcountries.eu/rest/v2/region/{}"
    
    headers = {
        'x-rapidapi-host': "restcountries-v1.p.rapidapi.com",
        'x-rapidapi-key': "DWDNu2VfVMmshDG6ZHL3h28QwJXwp1v13kxjsnIy0nGR6iT7Hm"
        }
    
    
    def get_country_language(self, region):
        url = self.url_countries.format(region)
        response = requests.request("GET", url, headers=self.headers)
        r = response.json()[0]
        country = r['name']
        language = languages.get(alpha2=r['languages'][0]['iso639_1']).name
        l_sha=  hashlib.sha1(language.encode())
        return {"Region":region, "CityName":country, "Languaje":l_sha.hexdigest(),
                "Time":timeit("...")}
    
    
    def create_table(self):
        response = requests.request("GET", self.url, headers=self.headers)    
        list_regions = list(map(lambda x: x['region'],response.json()))
        list_regions = list(set(filter(None, list_regions)))
        list_total  = list(map(self.get_country_language, list_regions )) 
        table = pd.DataFrame.from_dict(list_total)
        return table
    
    def get_report_time(self, data):    
        total_time = data['Time'].sum()
        mean_time = data['Time'].mean()
        max_time = data['Time'].max()
        min_time =  data['Time'].min()
        logging.info("Total time : {}".format(total_time))
        logging.info("Mean time : {}".format(mean_time))
        logging.info("Maximum time : {}".format(max_time))
        logging.info("Minimum time : {}".format(min_time))
        return total_time, mean_time, max_time, min_time
    
    def save_sqlite(self, data):
        try:
            conn = sqlite3.connect(r'files/data.sqlite')
        except sqlite3.OperationalError:
            os.mkdir('files')
        
        data.to_sql('data', conn, if_exists='replace', index=False)
    
    def create_json(self,data):
        if not os.path.exists('files/'):
            os.mkdir('files')
        file_name = 'data.json'
        json_t = data.to_json(orient='records')
        file = open('files/{}'.format(file_name),'w')
        file.write(str(json_t))
        file.close()
        return file_name
        

        
zinobe = ZinobeApp()   
logging.info("Create Datatable")
table = zinobe.create_table()
logging.info("Generate Time Reports")
zinobe.get_report_time(table)
logging.info("Save in sqlite")
zinobe.save_sqlite(table)
logging.info("Create Json")
zinobe.create_json(table)


